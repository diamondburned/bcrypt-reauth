package bcrypt

import (
	"net/http"

	"github.com/freman/caddy-reauth/backend"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

// Simple is the second simplest backend for authentication, a name:bcryptHash
// map
type BCryptStore map[string][]byte

func init() {
	err := backend.Register("bcrypt", Main)
	if err != nil {
		panic(err)
	}
}

func Main(config string) (backend.Backend, error) {
	return constructor(config)
}

func constructor(config string) (BCryptStore, error) {
	options, err := backend.ParseOptions(config)
	if err != nil {
		return nil, err
	}

	var store = make(BCryptStore, len(options))

	for k, v := range options {
		hash := []byte(v)

		// Validate hash
		_, err := bcrypt.Cost(hash)
		if err != nil {
			return nil, errors.Wrap(err, "Invalid hash for "+k)
		}

		store[k] = hash
	}

	return store, nil
}

func (h BCryptStore) Verify(username, password string) (bool, error) {
	bc, ok := h[username]
	if !ok {
		return false, nil
	}

	pwBytes := []byte(password)

	if err := bcrypt.CompareHashAndPassword(bc, pwBytes); err != nil {
		if err == bcrypt.ErrMismatchedHashAndPassword {
			return false, nil
		}

		return false, err
	}

	return true, nil
}

// Authenticate fulfils the backend interface
func (h BCryptStore) Authenticate(r *http.Request) (bool, error) {
	username, password, ok := r.BasicAuth()
	if !ok {
		return false, nil
	}

	return h.Verify(username, password)
}
