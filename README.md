# bcrypt-reauth

Use with [http.reauth](https://caddyserver.com/v1/docs/http.reauth).

## Usage

1. `import _ "gitlab.com/diamondburned/bcrypt-reauth"`
2. Caddyfile

```
reauth {
	bcrypt username=$2a$10$B9flFjG9161tEB0wChCdB.S.B378iKLrDNSgT2Ak4pBRZqmLmP9Tu
}
```

