module gitlab.com/diamondburned/bcrypt-reauth

go 1.13

require (
	github.com/freman/caddy-reauth v0.0.0-20191025011741-deaa60e56872
	github.com/pkg/errors v0.8.1
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
)
