package bcrypt

import (
	"testing"

	"golang.org/x/crypto/bcrypt"
)

const password = "test_password"

var hash, _ = bcrypt.GenerateFromPassword([]byte(password),
	bcrypt.DefaultCost)

func TestConstructor(t *testing.T) {
	config := `username=` + string(hash)

	s, err := constructor(config)
	if err != nil {
		t.Fatal("Failed to parse config", err)
	}

	pass, err := s.Verify("username", password)
	if err != nil {
		t.Fatal("Failed to verify", err)
	}

	if !pass {
		t.Fatal("Unexpected incorrect password")
	}
}
