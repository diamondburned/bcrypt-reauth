package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"golang.org/x/crypto/bcrypt"
)

func main() {
	cost := flag.Int("cost", bcrypt.DefaultCost, "Cost to hash bcrypt")
	flag.Parse()

	if len(flag.Args()) != 1 {
		log.Fatalln("usage:", os.Args[0], "[-cost n] <password>")
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(flag.Arg(0)), *cost)
	if err != nil {
		log.Fatalln("Error hashing:", err)
	}

	fmt.Println(string(hash))
}
